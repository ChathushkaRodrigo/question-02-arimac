	
FROM node:16
	
WORKDIR /app

COPY ./package.json ./package.json
COPY ./public ./public

	
COPY ./src ./src
COPY ./jsconfig.json ./jsconfig.json
COPY ./public ./public
COPY ./tailwind.config.js ./tailwind.config.js

	
RUN npm install

	
EXPOSE 3000

CMD ["npm","start"]
